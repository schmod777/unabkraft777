```
    As I wrote, it's merely a toy simulating the behavior of... you know, which
starts a running of LEDs, before a buzzer blows up in a frightening ``bip!''.
However, the board could be used for lots of simple RTD tricks.

    For those who do hacks on a keyboard after checking their hands are clean,
I mean living on OpenBSD (krkrkr), ``avrdude'' exists in the 'ports'.
So, after executing a ``make'' in the dev directory ('cause ``arduinoproject''
creates a sane Makefile), the following line (*) could give a hand for
uploading the code in the Arduino ("nano", I didn't mentioned it, just read the
header of the [sourceCode].ino) :

#======================upload.sh=========================#
#!/bin/sh
# To be loaded from the current project directory !
avrdude -p atmega328p -P /dev/cuaU0 -c stk500v1 -b 57600        (*)
-U flash:w:applet/unabkraft.hex
#========================================================#

-- 
The code name's root ain't specifically an ethical allusion, so if you
really wanna figure out where it comes from :
just look for some well known mad mathematician, Ted Kaczynski.

I want to emphasise that I disclaim forcefully any kind of transformation
of the current project, that would deal with the PWM pins... no need for
further details. Better promote WMP ! http://wmp.tetalab.org/
```
