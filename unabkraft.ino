/******************************************************************************\
* author        : serguei_chmod77 (Arthur)                                     *
* codename      : unabkraft                                                    *
* testing board : unab77                                                       *
* processor     : at(mel)mega 328p                                             *
* date          : 07.08.2015                                                   *
* version       : 1.5.2                     ``Boolzor powered''                *
*                                           compiler : avrdude, OpenBSD 5.6    *
\******************************************************************************/


// Id of digital-pins by nicknames :
const int pushButt = 2;
const int ledBarA = 3, ledBarB = 4, ledBarC = 5, ledBarD = 6, ledBarE = 7;
const int ledBarF = 8, ledBarG = 9, ledBarH = 10, ledBarI = 11;
const int mainLed = 12, bell = 13;
// Some integer variables for arithmetical counts along loop() :
int i = 2000, k = 0, pb = 0, lb = 0, s = 0;

int lastDebounceTime = 0;
long ctDownSet = 0, dc = 0, cd = 0;
bool zor = true, modeState = false;

void defusing() {
    zor = false;
}

void setup(void) {
    // Pins input/output mode global setting :
    pinMode(mainLed, OUTPUT);
    pinMode(ledBarA, OUTPUT);
    pinMode(ledBarB, OUTPUT);
    pinMode(ledBarC, OUTPUT);
    pinMode(ledBarD, OUTPUT);
    pinMode(ledBarE, OUTPUT);
    pinMode(ledBarF, OUTPUT);
    pinMode(ledBarG, OUTPUT);
    pinMode(ledBarH, OUTPUT);
    pinMode(ledBarI, OUTPUT);
    pinMode(bell, OUTPUT);
    pinMode(pushButt, INPUT);
    attachInterrupt(0, defusing, CHANGE);
}

void thisIsTheEnd() {

    i = 2000;
    while (i >= 400) {
        if (zor) {
            k=i/2;
            s = i - 50;
            digitalWrite(mainLed, HIGH);
            digitalWrite(bell, HIGH);
            delay(50);
            digitalWrite(bell, LOW);
            delay(s);
            digitalWrite(mainLed, LOW);
            delay(k);
            i -= k;
        }
        else {
            goto lastStraw;
        }
    }
    while (i >= 20) {
        if (zor) {
            digitalWrite(mainLed, HIGH);
            digitalWrite(bell, HIGH);
            delay(i);
            digitalWrite(mainLed, LOW);
            digitalWrite(bell, LOW);
            delay(i);
            i -= 10;
        }
        else {
            goto lastStraw;
        }
    }
    i = 2000;
    // Chenillard final
    lb = 3;
    while (lb < 12) {
        digitalWrite(lb, HIGH);
        delay(400);
        lb ++;
    }
    delay(500);
    lb = 3;
    while (lb < 12) {
        digitalWrite(lb, LOW);
        lb ++;
    }
    lb = 3;
    digitalWrite(bell, HIGH);
    while (lb < 12) {
        digitalWrite(lb, HIGH);
        delay(100);
        digitalWrite(lb, LOW);
        lb ++;
    }
    lb = 10;
    while (lb > 2) {
        digitalWrite(lb, HIGH);
        delay(100);
        digitalWrite(lb, LOW);
        lb --;
    }
    zor = true;

    lastStraw:
    digitalWrite(bell, LOW);

}

void initConditions() {
    delay(300); // interruption debounce, else it'll jump the 'while'
    lb = 3;
    while ( lb < 12 ) {
        digitalWrite(lb, LOW);
        lb ++;
    }
    ctDownSet = 0;
    k = 0;
    dc = 0;
    lastDebounceTime = 0;
    modeState = false;
}

/* ========================================================================== */
void loop(void) {
    clearSelect:
    initConditions();

    while ( modeState == false ) {
        if ( ctDownSet < 2700001 ) {
            k=ctDownSet/300000 + 2;
            digitalWrite(k, HIGH);

            if ( digitalRead(pushButt) == LOW ) {
                lastDebounceTime = millis();
                delay(300); // debounce
                            // check if the switch is pressed for a long time
                if( digitalRead(pushButt) == LOW && ( lastDebounceTime - millis() ) > 5000 ) {
                    modeState = true;
                    i=0;
                    while( i<2 ) {
                        digitalWrite(bell, HIGH);
                        delay(200);
                        digitalWrite(bell, LOW);
                        delay(200);
                        i++;
                    }
                    delay(700);
                    goto sayByBye;
                }
                // otherwise (short pressure), just increment the countdown delay
                else {
                ctDownSet += 300000;
                digitalWrite(mainLed, HIGH);
                delay(300);
                digitalWrite(mainLed, LOW);
                }
            }
        }
        else {
            goto clearSelect;
        }
    }

    sayByBye:
    zor = true;
    if (zor) {
        if ( ctDownSet != 0 ) {
            k=ctDownSet/300000;
            while ( k > 1 ) {
                //delay(300000);
                dc = 0;

                countDown:

                while ( dc < 300001 ) {
                    if (zor) {
                        delay(10);
                        dc += 10;
                    }
                    else {
                        i = 0;
                        while ( i < 2 ) {
                            digitalWrite(mainLed, HIGH);
                            delay(300);
                            digitalWrite(mainLed, LOW);
                            delay(300);
                            i ++;
                        }
                        cd = 0;
                        while ( cd < 10001 ) {
                            delay(10);
                            cd += 10;
                        }
                        zor = true;
                        delay(400);
                        if (zor) {
                            i = 0;
                            while ( i < 2 ) {
                                digitalWrite(mainLed, HIGH);
                                delay(300);
                                digitalWrite(mainLed, LOW);
                                delay(300);
                                i ++;
                            }
                            goto countDown;
                        }
                        else {
                            goto jackFlash;
                        }
                    }
                }
                digitalWrite(k+2, LOW);
                digitalWrite(k+1, HIGH);
                k --;
            }

            dc = 0;

            takeFive:

            while ( dc < 300001 ) {
                if (zor) {
                    delay(10);
                    dc += 10;
                }
                else {
                    i = 0;
                    while ( i < 2 ) {
                        digitalWrite(mainLed, HIGH);
                        delay(300);
                        digitalWrite(mainLed, LOW);
                        delay(300);
                        i ++;
                    }
                    cd = 0;
                    while ( cd < 10001 ) {
                        delay(10);
                        cd += 10;
                    }
                    zor = true;
                    delay(400);
                    if (zor) {
                        i = 0;
                        while ( i < 2 ) {
                            digitalWrite(mainLed, HIGH);
                            delay(300);
                            digitalWrite(mainLed, LOW);
                            delay(300);
                            i ++;
                        }
                        goto takeFive;
                    }
                    else {
                        goto jackFlash;
                    }
                }
            }
            digitalWrite(3, LOW);
        }
    }

    jackFlash:

    if (zor) {
        thisIsTheEnd();
        if ( zor == false ) {
            k = 11;
            while ( k > 2 ) {
                digitalWrite(k, HIGH);
                k --;
            }
            k = 9;
            goto defuseSign;
        }
    }

    else {

        defuseSign:
        lb = k + 2;
        while ( lb > 2 ) {
            digitalWrite(bell, HIGH);
            digitalWrite(lb, LOW);
            delay(500 - lb*25);
            digitalWrite(bell, LOW);
            delay(200 - lb*15);
            lb --;
        }

    }

    return;
}
